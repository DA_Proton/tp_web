<?php

require "vendor/autoload.php";
$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/views');
$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);
Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
});

Flight::route("/",function(){
    $data = [
        'dinos' => getDinos()
    ];
    Flight::view()->display("dinos.twig",$data);
});

Flight::route("/dinosaur/@dinoName", function($dinoName){
    $data = [
        'dinoDet' => getDinosDetails($dinoName)
    ];
    Flight::view()->display("details.twig", $data);
});

Flight::start();